import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import PlacesListScreen from "../screens/PlacesListScreen";
import PlacesDetailsScreen from "../screens/PlacesDetailsScreen";
import MapScreen from "../screens/MapScreen";
import NewplaceScreen from "../screens/NewplaceScreen";
import { Platform } from "react-native";
import { Color } from "../constants/Color";
import { RootStackParamList } from "./PlacesStackList";
const RootStack = createNativeStackNavigator<RootStackParamList>();

const PlacesNavigation = () => {
  return (
    <NavigationContainer>
      <RootStack.Navigator
        initialRouteName="Places"
        screenOptions={{
          headerStyle: {
            backgroundColor: Platform.OS === "android" ? Color.primary : "",
          },
          headerTintColor: Platform.OS === "android" ? "white" : Color.primary,
        }}
      >
        <RootStack.Screen name="Places" component={PlacesListScreen} />
        <RootStack.Screen name="PlaceDetails" component={PlacesDetailsScreen} />
        <RootStack.Screen name="NewPlaceScreen" component={NewplaceScreen} />
        <RootStack.Screen name="Map" component={MapScreen} />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

export default PlacesNavigation;
